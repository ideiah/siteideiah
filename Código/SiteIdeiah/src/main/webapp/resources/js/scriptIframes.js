// JavaScript Document
function MouseScroll(event) {
    infoMembro('close');
    var rolled = 0;
    if ('wheelDelta' in event) {
        rolled = event.wheelDelta;
    }
    else {  // Firefox
        // The measurement units of the detail and wheelDelta properties are different.
        rolled = -40 * event.detail;
    }
    if (rolled >= 120) {
        window.parent.rolagem(event, "up");
    } else {
        window.parent.rolagem(event, "down");
    }
    ;
}
function Init() {

    // for mouse scrolling in Firefox
    //var elem = document.getElementById ("myDiv");
    var elem = window.document.body;
    if (elem.addEventListener) {    // all browsers except IE before version 9
        // Internet Explorer, Opera, Google Chrome and Safari
        elem.addEventListener("mousewheel", MouseScroll, false);
        // Firefox
        elem.addEventListener("DOMMouseScroll", MouseScroll, false);
    }
    else {
        if (elem.attachEvent) { // IE before version 9
            elem.attachEvent("onmousewheel", MouseScroll);
        }
    }
}

var IMAGEM = 1;
var DIV = 2;

function trocaInfoSobre(DIV) {
    if (DIV === 1) {
        document.getElementById('textSobre03').style.opacity = "0";
        document.getElementById('textSobre02').style.opacity = "0";
        document.getElementById('textSobre04').style.opacity = "0";
        document.getElementById('textSobre01').style.opacity = "1";
        document.getElementById('spSobre3').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre2').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre4').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre1').style.backgroundColor = "#f1ca2f";
        IMAGEM = 1;
        transicaoImagens();
    }
    if (DIV === 2) {
        document.getElementById('textSobre03').style.opacity = "0";
        document.getElementById('textSobre01').style.opacity = "0";
        document.getElementById('textSobre04').style.opacity = "0";
        document.getElementById('textSobre02').style.opacity = "1";
        document.getElementById('spSobre1').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre3').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre4').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre2').style.backgroundColor = "#f1ca2f";
        IMAGEM = 2;
        transicaoImagens();
    }
    if (DIV === 3) {
        document.getElementById('textSobre01').style.opacity = "0";
        document.getElementById('textSobre02').style.opacity = "0";
        document.getElementById('textSobre04').style.opacity = "0";
        document.getElementById('textSobre03').style.opacity = "1";
        document.getElementById('spSobre1').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre2').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre4').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre3').style.backgroundColor = "#f1ca2f";
        IMAGEM = 3;
        transicaoImagens();
    }
    if (DIV === 4) {
        document.getElementById('textSobre01').style.opacity = "0";
        document.getElementById('textSobre02').style.opacity = "0";
        document.getElementById('textSobre03').style.opacity = "0";
        document.getElementById('textSobre04').style.opacity = "1";
        document.getElementById('spSobre1').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre2').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre3').style.backgroundColor = "#c1c1c1";
        document.getElementById('spSobre4').style.backgroundColor = "#f1ca2f";
        IMAGEM = 4;
        transicaoImagens();
    }
}
var X = 2;

function transicaoImagens() {
    switch (X) {
        case 1 :
            A = 1;
            B = 2;
            C = 3;
            D = 4;
            break;
        case 2 :
            A = 2;
            B = 3;
            C = 4;
            D = 1;
            break;
        case 3 :
            A = 3;
            B = 4;
            C = 1;
            D = 2;
            break;
        case 4 :
            A = 4;
            B = 1;
            C = 2;
            D = 3;
            break;
    }
    document.getElementById('img0' + D).style.opacity = "0";
    setTimeout(function () {
        document.getElementById('img0' + A).className = "img01";
    }, 310);
    setTimeout(function () {
        document.getElementById('img0' + B).className = "img02";
    }, 370);
    setTimeout(function () {
        document.getElementById('img0' + C).className = "img03";
    }, 430);
    setTimeout(function () {
        document.getElementById('img0' + D).className = "img04";
    }, 490);//passa pro final
    setTimeout(function () {
        document.getElementById('img0' + D).style.opacity = "1";
    }, 1000);
    X = A + 1;
    if (X === 5) {
        X = 1;
    }
}

function trocaInfo() {
    trocaInfoSobre(DIV);
    if (DIV < 4) {
        DIV = DIV + 1;
    } else {
        DIV = 1;
    }
}

var intervalo = window.setInterval(trocaInfo, 6000);

function infoMembro(ID) {
    if (ID == 'close') {
        //limpa a tela ao fechar o grid
        clearInterval(temporizador);
        clearInterval(piscaCursor);
        document.getElementById("targetFoto").innerHTML = '';
        document.getElementById("targetNome").innerHTML = '';
        document.getElementById("targetTexto").innerHTML = '';
        //
        document.getElementById('fundoGrid').className = "fundoGridOFF";
        document.getElementById('gridInfoEquipe').className = "gridInfoEquipeOFF";
        return;
    }
    document.getElementById('fundoGrid').className = "fundoGridON";
    document.getElementById('gridInfoEquipe').className = "gridInfoEquipeON";
}
cur = "ON";
function piscaCursor() {
    if (cur == "ON") {
        document.getElementById("cursor").style.opacity = "0";
        cur = "OFF";
        return;
    }
    if (cur == "OFF") {
        document.getElementById("cursor").style.opacity = "1";
        cur = "ON";
        return;
    }
}
pisca = setInterval(piscaCursor, 500);

function digitaGrid(ID, TMP, url) {
    //
    //trocar foto pela ID : membro_ID.jpeg
    //	
    document.getElementById("targetFoto").innerHTML = "<img src=\"" + url + "\" class=\"descGridimg\" />";
    //
    //
    nome = document.getElementById("nome_" + ID).innerHTML;
    curso = document.getElementById("curso_" + ID).innerHTML;
    cargo = document.getElementById("cargo_" + ID).innerHTML;
    descricao = document.getElementById("descricao_" + ID).innerHTML;
    TARGET = document.getElementById("targetTexto"); //nome do elemento que irá receber o conteudo
    TARGET_NOME = document.getElementById("targetNome"); //nome do elemento que irá receber o conteudo
    TARGET_NOME.innerHTML = "";
    TARGET.innerHTML = "";
    texto = "";
    texto += nome;
    texto += curso;
    texto += cargo;
    textCargo = nome.length + curso.length + 9; //inicio : "Cargo: | "
    textCargo2 = nome.length + curso.length + cargo.length; //fim
    difCargo = textCargo2 - textCargo;
    texto += descricao;
    i = 0;
    del = false;
    function escreve(texto) {
        if (del == true && ID == "151150950") {
            texto = texto.replace("Desenvolvedor||", "Estagiário no preparo do café||");
        }
        NUMCHAR = texto.length; //conta a quantidade de caracteres da string passada no paremetro
        if (i < nome.length && del == false) {
            if (texto.charAt(i) == '|') {
                TARGET_NOME.innerHTML += "<br/>";
            } else {
                TARGET_NOME.innerHTML += texto.charAt(i);
            }
        }
        else if (i > textCargo - 1 && i < textCargo2 && del == false) {
            if (texto.charAt(i) == '|') {
                TARGET.innerHTML += "<br/>";
            } else {
                TARGET.innerHTML += texto.charAt(i);
            }
        }
        else if (i >= textCargo2 && i < textCargo2 + difCargo && del == false && ID == "151150950") {
            remTxt = TARGET.innerHTML;
            remTxt = remTxt.substring(0, (remTxt.length - 1));
            TARGET.innerHTML = remTxt;
        }
        else if (i == textCargo2 + difCargo && del == false && ID == "151150950") {
            i = textCargo - 1;
            del = true;
        }
        else if (i > textCargo && del == true && ID == "151150950") {
            if (texto.charAt(i) == '|') {
                TARGET.innerHTML += "<br/>";
            } else {
                TARGET.innerHTML += texto.charAt(i);
            }
        }
        else if (texto.charAt(i) == '|') {
            TARGET.innerHTML += "<br/>";
        } else {
            TARGET.innerHTML += texto.charAt(i);
        }
        if (i == (NUMCHAR - 1)) {
            clearInterval(temporizador);
        }
        i = i + 1;
    }
    temporizador = setInterval(escreve, TMP, texto);
}

function lancadorGrid(ID, TMP, url) {
    setTimeout(digitaGrid, 1500, ID, TMP, url);
}

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "http://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'))
