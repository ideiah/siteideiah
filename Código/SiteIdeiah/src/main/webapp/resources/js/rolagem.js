

var TEMPSCROLL = true;
function rolagem(event, DIR) {
    var COM = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    var DIRECAO;
    if (COM == 40) {
        DIR = "down"
    }
    if (COM == 38) {
        DIR = "up"
    }

    // 1 -> Index
    // 2 -> Sobre
    // 3 -> Portifólio
    // 4 -> Notícias
    // 5 -> Contato	
    if (TEMPSCROLL == true) {
        var ATUAL = parseInt(readCookie('pagina')); //usei o parseInt() pq saporra nao tava decrementando
        if ((ATUAL < 4) && (DIR == "down")) {
            ATUAL = ATUAL + 1;
            posicionaConteudo(ATUAL);
        }
        if ((ATUAL > 0) && (DIR == "up")) {
            ATUAL = ATUAL - 1;
            posicionaConteudo(ATUAL);
        }
    }
    TEMPSCROLL = false;
    setTimeout(function () {
        TEMPSCROLL = true;
    }, 500);
}



// TROCA DE PAGINA COM O SCROLL DO MOUSE
function MouseScroll(event) {
    var iframePort = document.getElementById("iframePort");
    var conteudo = iframePort.contenDocument || iframePort.contentWindow.document;
    var flag = conteudo.getElementsByClassName('modal fade in');
    var rolled = 0;

    if (flag.length > 0) {
        return false;
    }

    if ('wheelDelta' in event) {
        rolled = event.wheelDelta;
    }
    else {  // Firefox
        // The measurement units of the detail and wheelDelta properties are different.
        rolled = -40 * event.detail;
    }

    if (rolled >= 120) {
        rolagem(event, "up")
    }
    ;

    if (rolled <= -120) {
        rolagem(event, "down")
    }
    ;

}

function Init() {
    // for mouse scrolling in Firefox
    //var elem = document.getElementById ("myDiv");
    var elem = window.document.body;
    if (elem.addEventListener) {    // all browsers except IE before version 9
        // Internet Explorer, Opera, Google Chrome and Safari
        elem.addEventListener("mousewheel", MouseScroll, false);
        // Firefox
        elem.addEventListener("DOMMouseScroll", MouseScroll, false);
    } else {
        if (elem.attachEvent) { // IE before version 9
            elem.attachEvent("onmousewheel", MouseScroll);
        }
    }
}

// ABRE E FECHA O MENU LATERAL
var aba;
aba = false;
function abreFechaMenu() {
    if (aba == false) {

        document.getElementById("ulMenu").style.display = "inherit";
        setTimeout(function () {
            document.getElementById("bt-menu").className = "bt-menu bt-menu-open";
        }, 400);

        aba = true;
    } else {
        document.getElementById("bt-menu").className = "bt-menu bt-menu-close";

        setTimeout(function () {
            document.getElementById("ulMenu").style.display = "none";
        }, 400);
        aba = false;
    }
}
