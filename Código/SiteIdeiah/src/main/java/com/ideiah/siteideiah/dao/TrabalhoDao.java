

package com.ideiah.siteideiah.dao;

import com.ideiah.siteideiah.model.Trabalho;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class TrabalhoDao extends Dao{
    
//<editor-fold defaultstate="collapsed" desc="Salvar">
    public boolean salvar(Trabalho trabalho) {
        return super.salvar(trabalho);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Buscar">
    public ArrayList<Trabalho> buscar() {
        return (ArrayList<Trabalho>) buscarObjetos(Trabalho.class);
    }

    public Trabalho buscar(int codigo) {
        return (Trabalho) buscarObjeto(codigo, Trabalho.class);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Deletar">
    public boolean deletar(int codigo) {
        return excluir(codigo, Trabalho.class);
    }
//</editor-fold>
}
