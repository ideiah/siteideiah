/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ideiah.siteideiah.control;

import java.util.Iterator;
import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

/**
 *
 * @author Pedro
 */
public class FacesUtil {
    
    public static void cleanSubmittedValues(UIComponent component) {
        if (component instanceof EditableValueHolder) {
            EditableValueHolder evh = (EditableValueHolder) component;
            evh.resetValue();
        }
        // Dependendo de como se implementa um Composite Component, ele retorna
        // ZERO
        // na busca por filhos. Nesse caso devemos iterar sobre os componentes
        // que o
        // compõe de forma diferente.
        if (UIComponent.isCompositeComponent(component)) {
            Iterator<UIComponent> i = component.getFacetsAndChildren();
            while (i.hasNext()) {
                UIComponent comp = (UIComponent) i.next();

                if (comp.getChildCount() > 0) {
                    for (UIComponent child : comp.getChildren()) {
                        cleanSubmittedValues(child);
                    }
                }
            }
        }

        if (component.getChildCount() > 0) {
            for (UIComponent child : component.getChildren()) {
                cleanSubmittedValues(child);
            }
        }
    }
}
