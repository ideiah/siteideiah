/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ideiah.siteideiah.control;

import com.ideiah.siteideiah.dao.TrabalhoDao;
import com.ideiah.siteideiah.model.Trabalho;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 *
 * @author Pedro
 */
@ManagedBean(name = "trabalhoBean")
@ViewScoped
public class TrabalhoBean {

    private List<Trabalho> trabalhosExistentes;
    private final TrabalhoDao trabalhoDao;
    private int primeiraPosicao;
    private Trabalho primeiroTrabalho;
    private Trabalho segundoTrabalho;
    private Trabalho terceiroTrabalho;
    private Trabalho quartoTrabalho;
    private Trabalho quintoTrabalho;
    private Trabalho sextoTrabalho;
    private FacesContext facesContext;
    private UIViewRoot uiViewRoot;

    /**
     * Creates a new instance of TrabalhoBean
     */
    public TrabalhoBean() {
        trabalhoDao = new TrabalhoDao();
        trabalhosExistentes = trabalhoDao.buscar();
        primeiraPosicao = 0;
        facesContext = FacesContext.getCurrentInstance();
        uiViewRoot = facesContext.getViewRoot();
        try {
            primeiroTrabalho = trabalhosExistentes.get(0);
            segundoTrabalho = trabalhosExistentes.get(1);
            terceiroTrabalho = trabalhosExistentes.get(2);
            quartoTrabalho = trabalhosExistentes.get(3);
            quintoTrabalho = trabalhosExistentes.get(4);
            sextoTrabalho = trabalhosExistentes.get(5);
        } catch (Exception e) {

        }
    }

    /**
     * @return the trabalhosExistentes
     */
    public List<Trabalho> getTrabalhosExistentes() {
        return trabalhosExistentes;
    }

    /**
     * @param trabalhosExistentes the trabalhosExistentes to set
     */
    public void setTrabalhosExistentes(List<Trabalho> trabalhosExistentes) {
        this.trabalhosExistentes = trabalhosExistentes;
    }

    public Trabalho getPrimeiroTrabalho() {
        return primeiroTrabalho;
    }

    public Trabalho getSegundoTrabalho() {
        return segundoTrabalho;
    }

    public Trabalho getTerceiroTrabalho() {
        return terceiroTrabalho;
    }
    
    public void limpaArvore(){
        FacesUtil.cleanSubmittedValues(uiViewRoot.findComponent("@form"));
    }

    public void atualizaTrabalhosFrente() {
        int contador = 0;
        try {
            primeiroTrabalho = trabalhosExistentes.get(primeiraPosicao + 6);
            contador++;
            segundoTrabalho = trabalhosExistentes.get(primeiraPosicao + 7);
            contador++;
            terceiroTrabalho = trabalhosExistentes.get(primeiraPosicao + 8);
            contador++;
            quartoTrabalho = trabalhosExistentes.get(primeiraPosicao + 9);
            contador++;
            quintoTrabalho = trabalhosExistentes.get(primeiraPosicao + 10);
            contador++;
            sextoTrabalho = trabalhosExistentes.get(primeiraPosicao + 11);
            contador++;
        } catch (Exception e) {
            if (contador > 4) {
                sextoTrabalho = null;
            } else if (contador > 3) {
                sextoTrabalho = null;
                quintoTrabalho = null;
            } else if (contador > 2) {
                sextoTrabalho = null;
                quintoTrabalho = null;
                quartoTrabalho = null;
            } else if (contador > 1) {
                sextoTrabalho = null;
                quintoTrabalho = null;
                quartoTrabalho = null;
                terceiroTrabalho = null;
            } else if (contador > 0) {
                sextoTrabalho = null;
                quintoTrabalho = null;
                quartoTrabalho = null;
                segundoTrabalho = null;
                terceiroTrabalho = null;
            }
        } finally {
            if (contador != 0) {
                primeiraPosicao = primeiraPosicao + 6;
            }
            System.out.println("Contador = " + contador);
            System.out.println("Trabalho 1 = " + primeiroTrabalho);
            System.out.println("Trabalho 2 = " + segundoTrabalho);
            System.out.println("Trabalho 3 = " + terceiroTrabalho);
            System.out.println("Posição é = " + primeiraPosicao);
        }
    }

    public void atualizaTrabalhosTras() {
        int contador = 0;
        try {
            primeiroTrabalho = trabalhosExistentes.get(primeiraPosicao - 6);
            contador++;
            segundoTrabalho = trabalhosExistentes.get(primeiraPosicao - 5);
            contador++;
            terceiroTrabalho = trabalhosExistentes.get(primeiraPosicao - 4);
            contador++;
            quartoTrabalho = trabalhosExistentes.get(primeiraPosicao - 3);
            contador++;
            quintoTrabalho = trabalhosExistentes.get(primeiraPosicao - 2);
            contador++;
            sextoTrabalho = trabalhosExistentes.get(primeiraPosicao - 1);
            contador++;
        } catch (Exception e) {
            if (contador > 4) {
                sextoTrabalho = null;
            } else if (contador > 3) {
                sextoTrabalho = null;
                quintoTrabalho = null;
            } else if (contador > 2) {
                sextoTrabalho = null;
                quintoTrabalho = null;
                quartoTrabalho = null;
            } else if (contador > 1) {
                sextoTrabalho = null;
                quintoTrabalho = null;
                quartoTrabalho = null;
                terceiroTrabalho = null;
            } else if (contador > 0) {
                sextoTrabalho = null;
                quintoTrabalho = null;
                quartoTrabalho = null;
                segundoTrabalho = null;
                terceiroTrabalho = null;
            }
        } finally {
            if (contador != 0) {
                primeiraPosicao = primeiraPosicao - 6;
            }
            System.out.println("Contador = " + contador);
            System.out.println("Trabalho 1 = " + primeiroTrabalho);
            System.out.println("Trabalho 2 = " + segundoTrabalho);
            System.out.println("Trabalho 3 = " + terceiroTrabalho);
            System.out.println("Posição é = " + primeiraPosicao);
        }
    }

    /**
     * @return the quartoTrabalho
     */
    public Trabalho getQuartoTrabalho() {
        return quartoTrabalho;
    }

    /**
     * @param quartoTrabalho the quartoTrabalho to set
     */
    public void setQuartoTrabalho(Trabalho quartoTrabalho) {
        this.quartoTrabalho = quartoTrabalho;
    }

    /**
     * @return the quintoTranalho
     */
    public Trabalho getQuintoTranalho() {
        return quintoTrabalho;
    }

    /**
     * @param quintoTranalho the quintoTranalho to set
     */
    public void setQuintoTranalho(Trabalho quintoTranalho) {
        this.quintoTrabalho = quintoTranalho;
    }

    /**
     * @return the sextoTrabalho
     */
    public Trabalho getSextoTrabalho() {
        return sextoTrabalho;
    }

    /**
     * @param sextoTrabalho the sextoTrabalho to set
     */
    public void setSextoTrabalho(Trabalho sextoTrabalho) {
        this.sextoTrabalho = sextoTrabalho;
    }
}
