// JavaScript Document
//######################### VARIAVEIS DE AMBIENTE ##############################

var animacaoAbertura = false;
var janelaAtiva;

if(readCookie('sessionId')!='IDEIAH'){
janelaAtiva = 0;
}else{janelaAtiva = readCookie('pagina');}



//######################### FUNÇÕES ##############################


//criar o cookie
function writeCookie(name,value,days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
            }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";

}
//ler o cookie
function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for(i=0;i < ca.length;i++) {
        c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return '';
}


//CENTRALIZAÇÃO DO LOGO PELO TAMANHO DA TELA DO NAVEGADOR
function ajusteLogo(){
	//testo se a pessoa ja esta no site e oculto abertura e animações
	if(readCookie('sessionId')=='IDEIAH'){
		document.getElementById('abertura').style.display="none";
		return false;}

	//fim
	if(animacaoAbertura==true){return;}

	var altura = window.innerHeight;
	var largura = window.innerWidth;
	
	//calculo das margens laterais em percentural dependendo
	var iw = 625;
	var ih = 309;
	var percW = (largura - iw)/2;
	var percH = (altura - ih)/2;
	
	percW = percW*100/largura + "%";
	percH = percH*100/altura + "%";
	percHI = ih*100/altura;  
	percWI = iw*100/largura;
	
//##	
	if((altura>ih) && (largura>iw)){
	document.getElementById('logoideiah').style.top = percH;
	document.getElementById('logoideiah').style.left = percW;
	}
//##
	if(largura<=iw){
	document.getElementById('logoideiah').style.top = percH;
	document.getElementById('logoideiah').style.left = '0%';	
	}
//##	
	if(altura<=ih){
	document.getElementById('logoideiah').style.top = '0%';
	document.getElementById('logoideiah').style.left = percW;		
	}
	document.getElementById('logoideiah').style.opacity = '1';
}


//CODIGOS DA ABERTURA

function bordRadius(){ 
	document.getElementById('topleft').style.borderBottomRightRadius = "100%";
	document.getElementById('topright').style.borderBottomLeftRadius = "100%";
	document.getElementById('basright').style.borderTopLeftRadius = "100%";
	document.getElementById('basleft').style.borderTopRightRadius = "100%";
}
function afasta(){
	document.body.style.overflow = "hidden";
	
	document.getElementById('topleft').style.transition = "2s linear";
	document.getElementById('topleft').style.marginRight = "100%";
	document.getElementById('topleft').style.marginBottom = "100%";

	document.getElementById('topright').style.transition = "2s linear";
	document.getElementById('topright').style.marginLeft = "100%";
	document.getElementById('topright').style.marginBottom = "100%";

	document.getElementById('basleft').style.transition = "2s linear";
	document.getElementById('basleft').style.marginRight = "100%";
	document.getElementById('basleft').style.marginTop = "100%";
	
	
	document.getElementById('basright').style.transition = "2s linear";
	document.getElementById('basright').style.marginLeft = "100%";
	document.getElementById('basright').style.marginTop = "100%";
}
function someMenu(){
	document.getElementById('menuAbertura').style.opacity = "0";
}
function someLogo(){
	document.getElementById('logoideiah').style.opacity = "0";	
}


function abertura(){
	//testo se a pessoa ja esta no site e oculto abertura e animações
	if(readCookie('sessionId')=='IDEIAH'){
		document.getElementById('abertura').innerHTML = "";
		document.getElementById('abertura').display="none";
		return false;}
	//fim 
	
	animacaoAbertura = true;
	
	someMenu();
	setTimeout(bordRadius,700);
	setTimeout(afasta,1200);
	setTimeout(someLogo,1500); 
	//apos 2 segundos da abertura ele cria o cookie para que nao aconteça novas animações caso o usuário nao saia do site:
	if(readCookie('sessionId')!='IDEIAH'){
	setTimeout(function(){writeCookie('sessionId', 'IDEIAH', 0);},2000);
	//setTimeout(function(){writeCookie('pagina', 0, 0);},2000);
	}
	setTimeout(function(){document.getElementById('abertura').innerHTML = "";
		document.getElementById('abertura').display="none";},2400);
}

//CONFIGURAÇÃO DAS TELAS DE CONTEUDOS: SOBRE, PORTIFOLIO, CONTATO

function divisaoTelas(){
	var alturaJanela = window.innerHeight;
	
	// configura o heigth de cada tela com a altura da tela do navegador:
	document.getElementById('t1').style.height = alturaJanela+"px";
	document.getElementById('t2').style.height = alturaJanela+"px";
	document.getElementById('t3').style.height = alturaJanela+"px";
	document.getElementById('t4').style.height = alturaJanela+"px";
	document.getElementById('t5').style.height = alturaJanela+"px";
	
	posicionaConteudo(janelaAtiva);
	
}	
	/*
	preciso que a janela se reposicione depois de se ajustar ao tamanho do rezise!
	
	
	*/
function posicionaConteudo(TELA){
	
	var janela = window.innerHeight;
	//Valores para TELA
	// 0 -> Index
	// 1 -> Sobre
	// 2 -> Portifólio
	// 3 -> Notícias
	// 4 -> Contato
	//
	document.getElementById('conteudo').style.transition="1s linear";
	switch (TELA){
	case 0: document.getElementById('conteudo').style.top="0px"; janelaAtiva = TELA; writeCookie('pagina', 0, 0);
	break;
	case 1: document.getElementById('conteudo').style.top="-"+(TELA*janela)+"px"; janelaAtiva = TELA; writeCookie('pagina', 1, 0);
	break;
	case 2: document.getElementById('conteudo').style.top="-"+(TELA*janela)+"px"; janelaAtiva = TELA; writeCookie('pagina', 2, 0);
	break;
	case 3: document.getElementById('conteudo').style.top="-"+(TELA*janela)+"px"; janelaAtiva = TELA; writeCookie('pagina', 3, 0);
	break;
	case 4: document.getElementById('conteudo').style.top="-"+(TELA*janela)+"px"; janelaAtiva = TELA; writeCookie('pagina', 4, 0);
	break;
	default : if(janelaAtiva==0){ 
				document.getElementById('conteudo').style.transition="0s linear";
				document.getElementById('conteudo').style.top="0px";
				setTimeout(function(){document.getElementById('conteudo').style.transition="0.5s linear";},1000);
				}else{
				document.getElementById('conteudo').style.transition="0s linear";	
				document.getElementById('conteudo').style.top="-"+(janelaAtiva*janela)+"px"
				setTimeout(function(){document.getElementById('conteudo').style.transition="0.5s linear";},1000);				
				};
	
	}
	 document.getElementById('conteudo').style.display="inherit";
	// muito util pra pegar uma coordenada de um objeto na tela:
	//var contato = document.getElementById('contato').getBoundingClientRect().top; //no caso estou pegando a posição em relação ao topo
}

var TEMPSCROLL = true;
function rolagem(event,DIR){
	var COM = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
	var DIRECAO;
	if(COM==40){DIR="down"}
	if(COM==38){DIR="up"}
	
	// 1 -> Index
	// 2 -> Sobre
	// 3 -> Portifólio
	// 4 -> Notícias
	// 5 -> Contato	
	if(TEMPSCROLL==true){
	var ATUAL = parseInt(readCookie('pagina')); //usei o parseInt() pq saporra nao tava decrementando
	if((ATUAL<3)&&(DIR=="down")){ATUAL = ATUAL + 1; posicionaConteudo(ATUAL);}
	if((ATUAL>0)&&(DIR=="up")){ATUAL = ATUAL - 1; posicionaConteudo(ATUAL);}
	}
	TEMPSCROLL=false;
	setTimeout(function(){TEMPSCROLL=true},500);
	}
	


// TROCA DE PAGINA COM O SCROLL DO MOUSE
function MouseScroll (event) {
var rolled = 0;
if ('wheelDelta' in event) {
	rolled = event.wheelDelta;
	}
	else {  // Firefox
			// The measurement units of the detail and wheelDelta properties are different.
	rolled = -40 * event.detail;
	}
	
if(rolled >= 120){rolagem(event,"up")};

if(rolled <= -120){rolagem(event,"down")};

	}

function Init () {
	// for mouse scrolling in Firefox
	 //var elem = document.getElementById ("myDiv");
	var elem = window.document.body;
	if (elem.addEventListener) {    // all browsers except IE before version 9
	// Internet Explorer, Opera, Google Chrome and Safari
	elem.addEventListener ("mousewheel", MouseScroll, false);
	// Firefox
	elem.addEventListener ("DOMMouseScroll", MouseScroll, false);
	}else {
		if (elem.attachEvent) { // IE before version 9
			elem.attachEvent ("onmousewheel", MouseScroll);
			}
	}
}

// ABRE E FECHA O MENU LATERAL
var aba;
aba = false;
function abreFechaMenu(){
	if(aba == false){
	
	document.getElementById("ulMenu").style.display="inherit";
	setTimeout(function(){document.getElementById("bt-menu").className="bt-menu bt-menu-open";},400);
	
	aba = true;
	}else{
	document.getElementById("bt-menu").className="bt-menu bt-menu-close";
	
	setTimeout(function(){document.getElementById("ulMenu").style.display="none";},400);
	aba = false;
	}
}